import junit.framework.TestCase;

public class RevenueTest extends TestCase {
	
	//Test number: 1
	//Objective: To test if a negative value will give me an Error
	//Test Input(s): calculateProfit -100
	//Test Expected Output(s): Error please enter a positive value to calculate
	
	public void testcalculateProfit001(){
	
		assertEquals(-1, Revenue.calculateProfit(-100.0, 0.0),0.02);
		
	}
	
	//Test number: 2
	//Objective: To test if an invalid amount for costs will give the error "-55"
	//Test Input(s): calculateProfit -50
	//Test  Expected Output(s): -55
	
	public void testcalculateProfit002() {
		
		assertEquals(-55, Revenue.calculateProfit(0.0, -50.0), 0.02);
	}
	
	//Test number: 3
	//Objective: To test if an invalid number for target will give the error "-66"
	//Test Input(s): calculateProfit 500
	//Test Expected Output(s): -66
	
	public void testcalculateProfit003() {
		
		assertEquals(-66, Revenue.calculateProfit(500.0, 0.0), 0.02);
	}
	
	//Test number: 4
	//Objective: To test if a valid number for costs gives no errors
	//Test Input(s): calculateProfit 2000
	//Test Expected Output(s): 3400.00
	
	public void
}


